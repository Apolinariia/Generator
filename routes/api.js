const express = require('express');
const router = express.Router();
const createError = require('http-errors');

const generators = require('../src/generators');
const GeneratorManager = require('../src/generatorManager');
const Ean13Barcode = require('../src/barcodes/ean13');

const gm = new GeneratorManager(generators);

router.get('/', function(req, res, next) {
    res.json(gm.all());
});

router.get('/barcode/ean13/:number.svg', function(req, res, next) {
    const code = new Ean13Barcode(req.params.number);
    res.contentType('image/svg+xml');
    res.send('<?xml version="1.0" encoding="UTF-8"?>' + code.toSvg());
});

router.get('/:country/:generator', function(req, res, next) {
    try {
        return res.json(gm.generate(req.params.country, req.params.generator, req.query));
    } catch (e) {
        return res.status(e.message.indexOf('not found') > -1 ? 404 : 400).json({
            error: e.message,
        });
    }
});

router.get('/:country/:generator/:number', function(req, res, next) {
    try {
        return res.json({
            valid: true,
            data: gm.validate(req.params.country, req.params.generator, req.params.number),
        });
    } catch (e) {
        if (e.message.indexOf('not found') > -1) {
            return res.status(404).json({
                error: e.message,
            });
        }

        return res.status(400).json({
            valid: false,
            reason: e.message,
        });
    }
});

module.exports = router;
