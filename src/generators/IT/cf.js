const Generator = require('../../abstractGenerator');
const {randRange, randomElement, diffYears, randomise} = require('../../helpers');
const handleParams = require('../../handleParams');
const {upper, digits} = require('../../sets');
const placeCodes = require('./placeCodes.json');

module.exports = class CF extends Generator {
    static get monthLetters() {
        return 'ABCDEHLMPRST';
    }

    static get weightsOdd() {
        return {
            '0': 1, '1': 0, '2': 5, '3': 7, '4': 9, '5': 13, '6': 15, '7': 17, '8': 19, '9': 21,
            'A': 1, 'B': 0, 'C': 5, 'D': 7, 'E': 9, 'F': 13, 'G': 15, 'H': 17, 'I': 19, 'J': 21,
            'K': 2, 'L': 4, 'M': 18, 'N': 20, 'O': 11, 'P': 3, 'Q': 6, 'R': 8, 'S': 12, 'T': 14,
            'U': 16, 'V': 10, 'W': 22, 'X': 25, 'Y': 24, 'Z': 23,
        };
    }

    static get weightsEven() {
        return {
            '0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
            'A': 0, 'B': 1, 'C': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6, 'H': 7, 'I': 8, 'J': 9,
            'K': 10, 'L': 11, 'M': 12, 'N': 13, 'O': 14, 'P': 15, 'Q': 16, 'R': 17, 'S': 18, 'T': 19,
            'U': 20, 'V': 21, 'W': 22, 'X': 23, 'Y': 24, 'Z': 25,
        }
    }

    params() {
        return {
            surname: {type: 'string'},
            firstname: {type: 'string'},
            birthdate: {
                type: 'string',
                pattern: '^(18|19|20|21|22)[0-9][0-9](-[0-9][0-9])?(-[0-9][0-9])?$',
            },
            gender: {
                type: 'string',
                values: ['m', 'f', 'mf'],
                default: 'mf',
            },
            birthplace: {
                type: 'string',
                pattern: '^[ABCDEFGHILMZ][0-9]{3}$',
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        const surname = params.surname ? this._encodeName(params.surname) : randomise(upper, 3);
        const firstname = params.firstname ? this._encodeName(params.firstname, true) : randomise(upper, 3);

        let [y, m, d] = this._parseDateRequirement(params.birthdate);
        if (!y) {
            y = randRange(0, 100);
        }
        y = y % 100;
        if (!m) {
            m = randRange(1, 13);
        }
        if (!d) {
            d = randRange(1, 29);
        }

        const gender = params.gender === 'mf' ? randomElement(['m', 'f']) : params.gender;
        if (gender === 'f') {
            d += 40;
        }

        const birthplace = params.birthplace || this._randomPlace();

        const code = surname + firstname
            + y.toString().padStart(2, '0') + CF.monthLetters[m - 1] + d.toString().padStart(2, '0')
            + birthplace;

        return code + this._calcChecksum(code);
    }

    _encodeName(name, isFirstName = false) {
        const consonants = [];
        const vowels = [];

        for (let letter of name.toUpperCase().split('')) {
            if ('BCDFGHJKLMNPQRSTVWXZ'.indexOf(letter) > -1) {
                consonants.push(letter);
            }
            if ('AEIOUY'.indexOf(letter) > -1) {
                vowels.push(letter);
            }
        }

        if (isFirstName && consonants.length > 3) {
            consonants.splice(1, 1);
        }

        let code = consonants.slice(0, 3);

        if (code.length < 3) {
            code = code.concat(vowels.slice(0, 3 - code.length));
        }

        for (let i = code.length; i < 3; i++) {
            code.push('X');
        }

        return code.join('');
    }

    _randomPlace() {
        const letter = randomElement(Object.keys(placeCodes));
        const [min, max] = randomElement(placeCodes[letter]);

        return letter + randRange(min, max + 1).toString().padStart(3, '0');
    }

    validate(value) {
        const match = value.match(/^([A-Z]{3})([A-Z]{3})(\d\d)([ABCDEHLMPRST])(\d\d)([A-Z]\d\d\d)([A-Z])$/);

        if (!match) {
            throw new Error('format')
        }

        const [, surname, firstname, year, monthLetter, dayGender, birthplace, checksum] = match;

        const [day, gender] = dayGender < 40 ? [dayGender, 'm'] : [dayGender - 40, 'f'];

        const currentYear = (new Date()).getFullYear() % 100;

        const y = year < currentYear ? '20' + year : '19' + year;
        const m = CF.monthLetters.indexOf(monthLetter) + 1;
        const birthdate = new Date(`${y}-${m.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')}T00:00:00Z`);

        if (!this._validatePlace(birthplace)) {
            throw new Error('placeCode');
        }

        const expectedChecksum = this._calcChecksum(value.substr(0, value.length - 1));

        if (checksum !== expectedChecksum) {
            throw new Error('checksum');
        }

        return {surname, firstname, birthdate, age: diffYears(new Date(), birthdate), gender, birthplace};
    }

    _calcChecksum(value) {
        let sum = 0;
        for (let i = 0; i < value.length; i++) {
            let n = value[i];
            sum += i % 2 === 0 ? CF.weightsOdd[n] : CF.weightsEven[n];  // because 0-indexed
        }

        return upper.charAt(sum % 26);
    }

    _parseDateRequirement(date) {
        if (date === '') {
            return [undefined, undefined, undefined];
        }

        if (date.match(/^\d{4}$/g)) {
            return [parseInt(date), undefined, undefined];
        }

        if (date.match(/^\d{4}-\d{2}$/g)) {
            return [parseInt(date.slice(0, 4)), parseInt(date.slice(5, 7)), undefined];
        }

        if (date.match(/^\d{4}-\d{2}-\d{2}$/g)) {
            return [parseInt(date.slice(0, 4)), parseInt(date.slice(5, 7)), parseInt(date.slice(8, 10))];
        }

        throw new Error('Invalid date requirement');
    }

    _validatePlace(code) {
        const letter = code.substr(0, 1);
        const number = parseInt(code.substr(1));

        if (placeCodes[letter] === undefined) {
            return false;
        }

        for (let [min, max] of placeCodes[letter]) {
            if (number >= min && number <= max) {
                return true;
            }
        }

        return false;
    }

    // __generatePlaceCodeRanges() {
    //     codes.sort();
    //     const o = {};
    //     for (let code of codes) {
    //         const letter = code.substr(0, 1);
    //         const number = parseInt(code.substr(1));
    //
    //         if (o[letter] === undefined) {
    //             o[letter] = [];
    //         }
    //
    //         if (o[letter].length === 0 || o[letter][o[letter].length - 1][1] !== number - 1) {
    //             o[letter].push([number, number]);
    //         } else {
    //             o[letter][o[letter].length - 1][1] = number
    //         }
    //     }
    //
    //     return o;
    // }
};
