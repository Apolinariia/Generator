const Generator = require('../../abstractGenerator');
const {randomise, arrayTimesArray, sum} = require('../../helpers');
const {digits} = require('../../sets');
const handleParams = require('../../handleParams');

module.exports = class BSN extends Generator {
    static get weights() {
        return [9, 8, 7, 6, 5, 4, 3, 2];
    }

    params() {
        return {
            length: {
                type: 'int',
                values: [8, 9],
                default: 9,
            },
        };
    }

    generate(params = {}) {
        params = handleParams(this.params(), params);

        let nr = randomise(digits, params.length - 1);
        while (nr[0] === '0') {
            nr = randomise(digits, params.length - 1);
        }

        const checksum = sum(arrayTimesArray(BSN.weights, nr.padStart(8, '0'))) % 11;

        if (checksum === 10) {
            return this.generate(params);
        }

        nr += checksum;

        return nr.slice(0, -5) + '.' + nr.slice(-5, -3) + '.' + nr.slice(-3);
    }

    validate(value) {
        value = value.replace(/[- \.]/g,'');
        if (!value.match(/^\d{8,9}$/g)) {
            throw new Error('format')
        }

        value = value.padStart(9, '0');

        if (sum(arrayTimesArray(BSN.weights, value.slice(0, 8))) % 11 !== parseInt(value[8])) {
            throw new Error('checksum')
        }

        return true;
    }
};
