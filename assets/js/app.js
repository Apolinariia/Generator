require('../css/app.scss');

require('./images');

require('bootstrap');  // using tooltip

require('./modules/clipboard');
require('./modules/generator');
require('./modules/grid');
require('./modules/scroll');
