const $ = require('jquery');
const t = require('../../helpers/translator');

$('.btn-clipboard').click(function() {
    const $el = $(this);

    this.parentNode.previousSibling.select();
    const result = document.execCommand('copy');

    $el.attr('title', result ? t('copy.success') : t('copy.failure'));
    $el.tooltip('show');

    setTimeout(function () {
        $el.tooltip('dispose');
    }, 3000);

    return false;
});
